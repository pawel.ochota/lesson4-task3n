const getInputValue = require('..');

describe('createList', () => {
  it('should returns a string when passed input has non-empty value', async () => {
    const input = document.createElement('input');
    input.value = 'test';
    const result = getInputValue(input);

    expect(typeof result).toBe('string');
    expect(result).toBe('test');
  });

  it('should returns a null when passed input has empty value', async () => {
    const input = document.createElement('input');
    const result = getInputValue(input);

    expect(result).toBe(null);
  });
});
